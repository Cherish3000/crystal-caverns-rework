/*
 * Copyright © 2004-2021 L2J DataPack
 *
 * This file is part of L2J DataPack.
 *
 * L2J DataPack is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J DataPack is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.l2jserver.datapack.ai.individual;

import static com.l2jserver.gameserver.config.Configuration.rates;

import java.util.ArrayList;
import java.util.List;

import com.l2jserver.datapack.ai.npc.AbstractNpcAI;
import com.l2jserver.gameserver.ai.CtrlIntention;
import com.l2jserver.gameserver.model.L2Object;
import com.l2jserver.gameserver.model.Location;
import com.l2jserver.gameserver.model.actor.L2Attackable;
import com.l2jserver.gameserver.model.actor.L2Npc;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.holders.SkillHolder;
import com.l2jserver.gameserver.model.skills.Skill;
import com.l2jserver.gameserver.network.NpcStringId;
import com.l2jserver.gameserver.network.clientpackets.Say2;

/**
 * Tears AI as part of Crystal Caverns Instance.
 * High Five confirmed retail behavior.
 * @author Cherish and L2JServer© Team 2021.
 */

public final class Tears extends AbstractNpcAI {

    private boolean isUsedInvulSkill = false;

    private int npcStatus = 0;

    private long dragonScaleStart = 0;
    private int dragonScaleNeed = 0;
    private int doorIsClosed = 0;

    private final List<L2Npc> copies = new ArrayList<>();

    private static final String damageTracker = "damageTracker";

    private static final int TEARS = 25534;
    private static final int TEARS_COPY = 25535;

    private static final int DRAGONSCALETIME = 3000;

    private static final int DOOR = 24220026;
    private static final int PORTAL = 32280;
    private static final Location ORACLE_GUIDE = new Location(144312, 154420, -11855);

    private static final int BLACK_SEED_OF_EVIL_SHARD = 9598;
    private static final int CONTAMINATED_CRYSTAL = 9690;
    private static final int CLEAR_CRYSTAL = 9697;

    private static final SkillHolder PRESENTATION = new SkillHolder(5441); // Presentation - Tears Mirror Image

    private static final Location[] TEARS_NEW_LOCATION = {
            new Location(143855, 154420, -11832),
            new Location(143945, 154688, -11832),
            new Location(144172, 154852, -11832),
            new Location(144451, 154852, -11832),
            new Location(144680, 154691, -11832),
            new Location(144766, 154423, -11832),
            new Location(144680, 154153, -11832),
            new Location(144452, 153990, -11832),
            new Location(144175, 153984, -11832),
            new Location(143946, 154153, -11832),
    };

    public Tears() {
        super(Tears.class.getSimpleName(), "ai/individual");
        addKillId(TEARS);
        addAttackId(TEARS, TEARS_COPY);
        addSkillSeeId(TEARS);
        addSpawnId(TEARS_COPY);
    }

    @Override
    public String onSkillSee(L2Npc npc, L2PcInstance caster, Skill skill, L2Object[] targets, boolean isSummon) {
        if (npc.isInvul() && (npc.getId() == TEARS) && (skill.getId() == 2369) && (caster != null)) {
            if (caster.getParty() == null) {
                return super.onSkillSee(npc, caster, skill, targets, isSummon);
            } else if (((dragonScaleStart + DRAGONSCALETIME) <= System.currentTimeMillis()) || (dragonScaleNeed <= 0)) {
                dragonScaleStart = System.currentTimeMillis();
                dragonScaleNeed = caster.getParty().getMemberCount() - 1;
            } else {
                dragonScaleNeed--;
            }
            if ((dragonScaleNeed == 0) && (getRandom(100) < 80)) {
                npc.setIsInvul(false);
                broadcastNpcSay(npc, Say2.NPC_SHOUT, NpcStringId.NOYOU_KNEW_MY_WEAKNESS);
            }
        }
        return super.onSkillSee(npc, caster, skill, targets, isSummon);
    }

    @Override
    public String onAttack(L2Npc npc, L2PcInstance attacker, int damage, boolean isSummon, Skill skill) {

        if (npc.getId() == TEARS) {

            npc.getVariables().set(damageTracker, npc.getVariables().getInt(damageTracker) + damage);

            if (npc.getVariables().getInt(damageTracker) > 100000) {
                for (L2Npc copy : copies) {
                    if (copy!= null) {
                        copy.deleteMe();
                    }
                }
            }

            if (npc.isInCombat() && doorIsClosed < 1) {
                closeDoor(DOOR, npc.getInstanceId());
                doorIsClosed = 1;
            }

            int maxHp = npc.getMaxHp();
            double nowHp = npc.getStatus().getCurrentHp();

            if (!isUsedInvulSkill) {
                if ((nowHp <= (maxHp * 0.8)) && (npcStatus == 0)) {
                    npcStatus = 1;
                    startQuestTimer("SpawnTearsCopies", 2000, npc, attacker);
                    npc.doCast(PRESENTATION);
                } else if ((nowHp <= (maxHp * 0.6)) && (npcStatus == 1)) {
                    npcStatus = 2;
                    startQuestTimer("SpawnTearsCopies", 2000, npc, attacker);
                    npc.doCast(PRESENTATION);
                } else if ((nowHp <= (maxHp * 0.4)) && (npcStatus == 2)) {
                    npcStatus = 3;
                    startQuestTimer("SpawnTearsCopies", 2000, npc, attacker);
                    npc.doCast(PRESENTATION);
                } else if ((nowHp <= (maxHp * 0.4)) && (npcStatus == 3)) {
                    npcStatus = 4;
                    startQuestTimer("SpawnTearsCopies", 2000, npc, attacker);
                    npc.doCast(PRESENTATION);
                } else if (nowHp <= (maxHp * 0.1)) {
                    isUsedInvulSkill = true;
                    npc.setIsInvul(true);
                    broadcastNpcSay(npc, Say2.NPC_SHOUT, NpcStringId.IT_WILL_NOT_BE_THAT_EASY_TO_KILL_ME);
                }
            }

        } else if (npc.getId() == TEARS_COPY) {
            npc.getVariables().set(damageTracker, npc.getVariables().getInt(damageTracker) + damage);
            if (npc.getVariables().getInt(damageTracker) > 50000) {
                npc.deleteMe();
            }
        }

        return null;
    }

    private void giveRewards(L2PcInstance player) {
        final int num = Math.max(rates().getCorpseDropChanceMultiplier().intValue(), 1);

        if (hasQuestItems(player, CONTAMINATED_CRYSTAL)) {
            takeItems(player, CONTAMINATED_CRYSTAL, 1);
            giveItems(player, CLEAR_CRYSTAL, 1);
            giveItems(player, BLACK_SEED_OF_EVIL_SHARD, num);
        }
    }

    @Override
    public String onKill(L2Npc npc, L2PcInstance player, boolean isSummon) {
        if (npc.getId() == TEARS) {
            addSpawn(PORTAL, ORACLE_GUIDE, false, 0, false, player.getInstanceId());
            giveRewards(player);
        }
        return "";
    }

    @Override
    public String onAdvEvent(String event, L2Npc npc, L2PcInstance attacker) {

        double hpStat = (npc.getStatus().getCurrentHp()) / (npc.getMaxHp());
        if (event.equalsIgnoreCase("SpawnTearsCopies")) {
            Location tearsNewSpawnLoc = TEARS_NEW_LOCATION[getRandom(TEARS_NEW_LOCATION.length)];
            npc.deleteMe();
            for (int i = 0; i < 10; i++) {
                Location currentSpawnLoc = TEARS_NEW_LOCATION[i];
                if (tearsNewSpawnLoc != currentSpawnLoc) {
                    L2Npc copy = addSpawn(TEARS_COPY, currentSpawnLoc, false, 0, true, attacker.getInstanceId());
                    copy.setCurrentHp(copy.getMaxHp() * hpStat);
                    ((L2Attackable) copy).addDamageHate(attacker, 0, 99999);
                    copies.add(copy);
                    startQuestTimer("ApplyAggressionCopies", 3000, copy, attacker);
                } else {
                    L2Npc realTears = addSpawn(TEARS, currentSpawnLoc, false, 0, true, attacker.getInstanceId());
                    realTears.setCurrentHp(realTears.getMaxHp() * hpStat);
                    ((L2Attackable) realTears).addDamageHate(attacker, 0, 99999);
                    startQuestTimer("ApplyAggressionTears", 3000, realTears, attacker);
                }
            }
            cancelQuestTimers("SpawnTearsCopies");
        } else if (event.equalsIgnoreCase("ApplyAggressionCopies")) {
            npc.setRunning();
            npc.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
        } else if (event.equalsIgnoreCase("ApplyAggressionTears")) {
            npc.setRunning();
            npc.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, attacker);
            cancelQuestTimers("ApplyAggressionTears");
        }

        return super.onAdvEvent(event, npc, attacker);
    }

}